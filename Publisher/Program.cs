﻿using System;
using EasyNetQ;
using Messages;
using Quartz;
using System.Net;
using System.Net.Mail;
using Quartz.Impl;

namespace Publisher
{
    class Program
    {
        static void Main(string[] args)
        {
            Start();
        }

        public class EmailSender : IJob
        {
            private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            public void Execute(IJobExecutionContext context)
            {
                using (var bus = RabbitHutch.CreateBus("host=localhost"))
                {

                    double input;
                    input = (DateTime.UtcNow - Jan1st1970).TotalMilliseconds;
                    bus.Publish(new TextMessage
                    {
                        Text = input
                    });
                }
            }
        }

        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<EmailSender>().Build();

            ITrigger trigger = TriggerBuilder.Create()  
                .WithIdentity("trigger1", "group1")     
                .StartNow()                           
                .WithSimpleSchedule(x => x            
                    .WithIntervalInSeconds(1)         
                    .RepeatForever())                   
                .Build();            

            scheduler.ScheduleJob(job, trigger);      
        }
    }
}