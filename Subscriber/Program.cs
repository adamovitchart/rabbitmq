﻿using System;
using EasyNetQ;
using Messages;
using System.Net;
using System.Net.Http;
using System.IO;
using System.Web.Script.Serialization;
using System.Text;

namespace Subscriber
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                bus.Subscribe<MyMessage>("myQueue", HandleTextMessage);

                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();

            }
        }

        static void HandleTextMessage(MyMessage textMessage)
        {
            
            /* Console.WriteLine("Got message: {0}", textMessage.Message); */
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://operationwithnumber.azurewebsites.net/api/HttpTriggerJS1?code=VJalZvwydflVdc7h8aIo8BN9OAMGa3tcvVK7p7nKhmfS6dQZyNGzjQ==");

            request.ContentType = "application/json";
            request.Method = "POST";

            string msgToSent = Convert.ToString(textMessage.Message);
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    myNumber = msgToSent
                });

                streamWriter.Write(json);
            }
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            var encoding = Encoding.GetEncoding(response.CharacterSet);
            string resp = "";
            using (var streamReader = new StreamReader(response.GetResponseStream(), encoding))
            {
                var result = streamReader.ReadToEnd();
                resp = result;
                Console.WriteLine("Encrypted number (by Polybius cipher): " + resp);
            }
            response.Close();
        }
    }
}